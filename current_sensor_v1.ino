#define BLYNK_PRINT Serial   
#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>
const int sensorIn = A0;
int mVperAmp = 100; // use 100 for 20A Module and 66 for 30A Module
double result;
double VRMS = 0;
double AmpsRMS;
int value_for_blynk = 0;
int value_for_current_sensor = 0;
int electrical_point_1; 
SimpleTimer timer;
char auth[] = "2tdyr93uhvXm6ohJ3glpEnP000XFzHJW";            
char ssid[] = "Raj";                                       
char pass[] = "raj@00321"; 

void setup() {
  Serial.begin(9600);
  Blynk.begin(auth, ssid, pass);
  timer.setInterval(600,getVPP);
}
BLYNK_WRITE(V8) {
  electrical_point_1 = param.asInt();
}

BLYNK_CONNECTED() {
value_for_current_sensor = 1;  
  }

void getVPP(){
  int readValue;             //value read from the sensor
  int maxValue = 0;          // store max value here
  int minValue = 1024;          // store min value here
  uint32_t start_time = millis();
  while((millis()-start_time) < 200) {  //sample for 1 Sec
    readValue = analogRead(sensorIn);
    if (readValue > maxValue){
      maxValue = readValue;
    }
    if (readValue < minValue){
      minValue = readValue;
    }
  }
  result = ((maxValue - minValue) * 3.3)/1024.0;
  VRMS = (result/2.0) *0.707; 
  AmpsRMS = (VRMS * 1000)/mVperAmp;
  Serial.printf("%2.3f",AmpsRMS);
  Serial.println(" Amps RMS");
  if(AmpsRMS < 0.05){         // 0.05 for 30A module
    Blynk.virtualWrite(8,"  OFF");
  }
  else{
    Blynk.virtualWrite(8,"  ON");
  }
   
   //Blynk.virtualWrite(7,AmpsRMS);
}

void loop() {
   Blynk.run();
   timer.run();
}
